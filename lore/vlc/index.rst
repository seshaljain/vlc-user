*********
About VLC
*********

`VLC media player <https://www.videolan.org/vlc/>`_ (commonly known as just **VLC**) is a free and open source cross-platform multimedia player and framework.

VLC plays most multimedia files as well as Blurays, DVDs, Audio CDs, VCDs, and various streaming protocols.

You can download the latest version of VLC on our `website <https://www.videolan.org/vlc/#download>`_ for free. 

Key Features
============

* VLC plays Files, Discs, Webcams, Devices and Streams.
* VLC runs on all platforms and is completely free. 
* It has the most complete feature-set over the video, subtitle synchronisation, video and audio filters.
* It has hardware decoding on most platforms. It supports 0-copy on the GPU and can fallback on software when required.
* No spywares, ads and user tracking are allowed on the VLC media player.
* Advanced formats are allowed on VLC.
* VLC lets you apply audio effects, video effects, and tweak the way a video’s audio and video line up.

VLC Codenames
=============

From the first version of VLC to 1.0., VLC codenames are based on characters in `Goldeneye <http://en.wikipedia.org/wiki/GoldenEye>`_.

This movie is one of the first movies that was used during the original development phase to test VLC, and therefore was selected. 

+-------------+-----------+
|     Version | Codename  |
+=============+===========+
| < 0.2.(x<5) | Onatopp   |
+-------------+-----------+
|  0.2.(x>=5) | Ourumov   |
+-------------+-----------+
|       0.3.x | Ourumov   |
+-------------+-----------+
|       0.4.x | Ourumov   |
+-------------+-----------+
|       0.5.x | Natalya   |
+-------------+-----------+
|       0.6.x | Trevelyan |
+-------------+-----------+
|       0.7.x | Bond      |
+-------------+-----------+
|       0.8.x | Janus     |
+-------------+-----------+
|       0.9.x | Grishenko |
+-------------+-----------+
|       1.0.x | Goldeneye |
+-------------+-----------+

Starting from 1.1.x and above, VLC codenames are based on characters from `DiscWorld <http://en.wikipedia.org/wiki/Discworld>`_, from `Terry Pratchett <http://en.wikipedia.org/wiki/Terry_Pratchett>`_.

VLC 2.2.1 was named "Terry Pratchett", as being the first release after his death. 

+---------------------+-----------------+
|             Version | Codename        |
+=====================+=================+
|               1.1.x | The Luggage     |
+---------------------+-----------------+
|               2.0.x | Twoflower       |
+---------------------+-----------------+
|               2.1.x | Rincewind       |
+---------------------+-----------------+
| 2.2.x except 2.2.1) | Weatherwax      |
+---------------------+-----------------+
|               2.2.1 | Terry Pratchett |
+---------------------+-----------------+
|               3.0.x | Vetinari        |
+---------------------+-----------------+
|               4.0.x | Otto Chriek     |
+---------------------+-----------------+

Work has already started on `world domination <https://trac.videolan.org/vlc/ticket/35>`_, which is scheduled for version 42.0.
