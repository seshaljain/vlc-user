################
 VLC Interfaces
################


Interfaces are the way you interact with . Like anything else in VLC, they are modules, which allows for their interchangeability.

Main interfaces
---------------

VLC has four main graphical interfaces:

-  The Qt Interface (qt) is the default interface on :doc:`Linux </gettingstarted/interface/windows_and_linux/index>` and :doc:`Windows </gettingstarted/interface/windows_and_linux/index>`.
-  The :doc:`skins2 Interface </addons/skins>` is an interface where you can customize VLC's look (works on Linux and Windows).
-  The :doc:`macOS Interface </gettingstarted/interface/mac/index>` is the default (and only) graphical interface on macOS.

Full List
---------

Besides the above main interfaces, VLC contains many more:

=============================== =============================================================================================================================
Interface                       Description
=============================== =============================================================================================================================
**qt**                          Default Qt4 interface on Linux and Windows.
**skins2**                      Load VLC with a skin (Linux and Windows only).
**macosx**                      Default Mac OSX interface.
**minimal_macosx**              Minimal Mac OSX interface.
**http**                        :doc:`Web Interface <remote/web>`, used for controlling VLC from over a network.
**gestures**                    :doc:`Mouse Gestures <mouse_gestures>`, where you can control VLC by moving the mouse
**rc**, **ncurses**, **telnet** :doc:`Console Interfaces <console>`, non-graphical interfaces.
**showintf**                    Show interfaces module.
**hotkeys** and **joystick**    Control VLC with the keyboard/joystick (see :doc:`HotKeys </basic/hotkeys>`).
**dummy**                       Don't use an interface (HotKeys still available).
=============================== =============================================================================================================================

Listing the available interfaces
--------------------------------

To get a list of available interfaces, running VLC with the ``-l`` option:

.. code-block:: console

                % vlc -l

This also displays the muxers and encoders/decoders and puts it in a file called ``vlc-help.txt``. On Linux, run

.. code-block:: console

                % vlc -l | grep -iF interface

to display the interfaces.

Using an interface
------------------

To run VLC with a different primary interface, use the following
command:

.. code:: console

          % vlc --intf [name]

You can also use

.. code:: console

          % vlc -T [name]

You can also change the default in the :doc:`Preferences </basic/settings/preferences>`.

However, you can also launch more than one interface:

.. code:: console

          % vlc --int qt --extraintf sap,telnet,http

This will launch VLC with the default Qt interface, but will also launch the SAP, telnet and :doc:`web interface <remote/web>` in addition to the Qt one. The default for this can also be changed in the preferences.

Note that if you only use the ``dummy`` interface, you won't be able to tell vlc to quit (except watching a video). You may have to break it manually with Ctrl+C; or use ``vlc://quit`` as the last item on the playlist.
