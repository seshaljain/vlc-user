####################
 Console Interfaces
####################

VLC has three terminal modules. These are *rc*, *telnet* and *ncurses*.

rc interface
------------

The **rc** module is an interactive command line interface. It allows you to type commands to make VLC do things. To start it, run ``vlc --intf rc``. This is the default interface if no GUI environment is available. To get started, type "help" followed by :kbd:`Enter`.

ncurses interface
-----------------

The **ncurses** module is something like a text-mode GUI, built with the well-known ``ncurses`` library. Linux users should be familiar with this kind of interface. This interface is not built by default, you need to add ``--enable-ncurses`` to the configure call. To start VLC with this interface run ``vlc --intf ncurses``.

You can access this interface through a network with a telnet-client by using the ``--rc-host localhost:port`` option.

telnet interface
----------------

The **telnet** interface will allow you to use the telnet command to connect to VLC remotely from the network. It is comparable to the rc interface, but less advanced.

**Note**: You should use the rc-interface if you don't need this feature.

To launch VLC with telnet interface run ``vlc -I telnet --telnet-password test`` on the command line.

The telnet interface is essentially over a "raw TCP socket". To interact with this instance at the command line (assuming the telnet interface's address is ``10.0.0.100:4212``) run:

.. code:: console

          % nc 10.0.0.100 4212
