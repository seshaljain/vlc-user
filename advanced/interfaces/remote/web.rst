.. _web:

##################################
 Control VLC with a Web Interface
##################################

You can control VLC Media Player with a web interface

.. figure:: /images/advanced/interfaces/remote/http-interface.png
   :align: center

- To use the HTTP interface, you need to enable it:

  1. Go to Tools > Preferences

  2. Under *Show settings*, select *All*

  3. In the list of settings, go to Interface > Main interfaces

  4. Select the http checkbox

  You can also launch VLC with the http interface by following instructions at the `Interfaces documentation </advanced/interfaces/introduction#using-an-interface>`_

  The web interface is password protected to prevent unauthorised usage.
  Set the password at *Tools > Preferences > All > Interface > Main interfaces > Lua > Lua HTTP > Password*

- Open the IP address of the host in a browser. You may need to specify the port (VLC defaults to using port ``8080``).

  In the dialog box, leave the *User Name* field blank, and enter the *Password* you set for the web interface.

You should now be able to control VLC via the web interface.

*Note: If you want to control VLC from a computer that is not the one running VLC then you will need to enable access in the* ``.hosts`` *file (from v0.9.6 onwards)*
