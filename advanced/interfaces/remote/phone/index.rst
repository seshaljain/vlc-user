.. _phone:

#############################
Control VLC with a Smartphone
#############################

.. toctree::
   :maxdepth: 1

   android
   iphone
