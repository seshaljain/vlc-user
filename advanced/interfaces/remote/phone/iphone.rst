.. _iphone:

############################
 Control VLC with an iPhone
############################

There are various options to control VLC from the iPhone. These broadly break down between methods that require a separate application to run alongside VLC and applications that communicate directly with VLC using the :doc:`http interface </advanced/interfaces/remote/web>`.

============================================
 Options that communicate directly with VLC
============================================

-  It is possible to use the http interface directly from Safari although is is somewhat cumbersome as it isn't designed for such a small screen.

   -  :doc:`Control VLC via a browser </advanced/interfaces/remote/web>`

-  VLC Mobile Remote on the `App Store <https://apps.apple.com/us/app/vlc-mobile-remote/id1140931401>`_ is a free application available on App Store to control VLC from iPhone, iPod & iPad devices. VLC Mobile Remote has an inbuilt guide to easily setup VLC and 'Auto Connection' feature in the app detects & connects to VLC without requiring to install additional helper application on Windows, Mac or Linux computers. Find more info about `VLC Mobile Remote <http://vlcmobileremote.com>`_ app on their website.

-  `Hobbyist Software <http://hobbyistsoftware.com/VLC-more>`_ have produced two native applications which allow control of VLC from within a normal iPhone app. They also provide `setup helper applications <http://www.hobbyistsoftware.com/VLCSetup>`_ for Windows and Mac OS which simplify the process of enabling the http interface in VLC.

   -  `VLC Remote Free <http://itunes.com/apps/vlcremotefree>`_ allows control of basic functions like play, pause, stop, volume, etc
   -  `VLC Remote <http://itunes.com/apps/vlcremote>`_ is a paid version which additionally allows selecting of files and playlist management as well as more advanced features like aspect ratio, subtitles, audio delays, etc

-  `Flvorful Games <http://flvorfulgames.com/velcro>`_ has produced a free native application called `Velcro <http://itunes.com/apps/velcro>`_ which allows control of VLC from within a normal iPhone app. They provide `instructions <http://www.flvorfulgames.com/velcro/support>`_ for Windows and Mac OS which simplify the process of setting up VLC.

Options that communicate via an intermediate connection
=======================================================

-  `Intelliremote <http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=288765964&mt=8>`_ is a free iPhone client for Intelliremote's `PC Server <http://melloware.com/products/intelliphone/>`_ (produced by Melloware). It requires the PC client to be running on the same Windows PC as VLC.

-  `EventGhost <http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=292947868&mt=8>`_ is a paid iPhone client for the `Event Ghost <http://www.eventghost.org/wiki/EventGhost:About>`_ open source home automation server for Windows. (The iPhone client is also produced by Melloware). It requires the PC client to be running on the same Windows PC as VLC.

-  `RedTouch <http://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=289687093&mt=8i>`_ is a paid iPhone client for an ethernet-connected infrared transceiver produced by `IRTrans <http://irtrans.com>`_
