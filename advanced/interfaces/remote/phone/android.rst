.. _android:

###################################
 Control VLC with an Android Phone
###################################


-  It is possible to use the http interface directly from the browser although is is somewhat cumbersome as it isn't designed for such a small screen.

   -  :doc:`Control VLC via a browser </advanced/interfaces/remote/web>`

-  VLC Mobile Remote on the `Play Store <https://play.google.com/store/apps/details?id=adarshurs.android.vlcmobileremote>`_ is a free Android app developed by Adarsh Urs with good user interface is easy to use, and includes all the basic as well as advanced controls of VLC.

   -  Watch full `VLC setup instructions video <https://www.youtube.com/watch?v=2wu1vQDazNY>`_ on Youtube from `VLC Mobile Remote <http://vlcmobileremote.com>`_.

-  `Hobbyist Software <http://www.hobbyistsoftware.com>`_ have produced two native applications which allow control of VLC from within a normal Android app. They also provide `setup helper applications <http://www.hobbyistsoftware.com/VLCSetup>`_ for Windows and Mac OS which simplify the process of enabling the http interface in VLC.

   -  `VLC Remote Free <http://hobbyistsoftware.com/android/vlc-remote-free>`_ allows control of basic functions like play, pause, stop, volume, etc
   -  `VLC Remote <http://hobbyistsoftware.com/android/vlc-remote>`_ is a paid version which additionally allows selecting of files and playlist management as well as more advanced features such as aspect ratio, subtitles, etc.

-  `VLC Direct Pro Free <https://play.google.com/store/apps/details?id=com.vlcforandroid.vlcdirectprofree>`_ is a free application (with ads) which allows to to control VLC remotely and stream from VLC to Android and vice versa.
